function setup()
{
	option=$1
	SRC_LOCATION=$HOME/sst_workspace/
	# common stuff
	case $option in
		0)
			echo "Clean!"
			return
		;;

		1)
			export SST_CORE_HOME=/opt/sst/sst-core
			export SST_ELEMENTS_HOME=/opt/sst/sst-elements
			export SST_PATHS=$SST_CORE_HOME/bin:$SST_ELEMENTS_HOME/bin
			export SST_PKGCONFIG_PATH=$SST_CORE_HOME/lib/pkgconfig
			export PKG_CONFIG_PATH=${SST_PKGCONFIG_PATH}:${PKG_CONFIG_PATH}
			echo "Selected sst-core+elements"
		;;

		*)
			echo "Invalid selection."
			return
		;;
	esac

	export MPIHOME=/opt/sst/OpenMPI-2.1.3
	export MPICC=mpicc
	export MPICXX=mpicxx
	export PATH=$MPIHOME/bin:$PATH
	export PATH=$SST_PATHS:$PATH

}

function cleanup()
{
	echo "Was already setup ... cleaning"
	export PKG_CONFIG_PATH=`echo ${PKG_CONFIG_PATH} | sed 's@'":${SST_PKGCONFIG_PATH}"'@@'`
	export PKG_CONFIG_PATH=`echo ${PKG_CONFIG_PATH} | sed 's@'"${SST_PKGCONFIG_PATH}:"'@@'`
	export PKG_CONFIG_PATH=`echo ${PKG_CONFIG_PATH} | sed 's@'"${SST_PKGCONFIG_PATH}"'@@'`
	export SST_PKGCONFIG_PATH=
	export PATH=`echo $PATH | sed 's@'":$MPIHOME/bin"'@@'`
	export PATH=`echo $PATH | sed 's@'"$MPIHOME/bin:"'@@'`
	export PATH=`echo $PATH | sed 's@'"$MPIHOME/bin"'@@'`
	export PATH=`echo $PATH | sed 's@'":$SST_PATHS"'@@'`
	export PATH=`echo $PATH | sed 's@'"$SST_PATHS:"'@@'`
	export PATH=`echo $PATH | sed 's@'"$SST_PATHS"'@@'`
	export SST_PATHS=
	export SST_CORE_HOME=
	export SST_ELEMENTS_HOME=
	export MPIHOME=
	export MPICC=
	export MPICXX=
}


# Clean up
if [ -n "$SST_PATHS" ]
then
	cleanup
fi


if [ -n "$1" ]
then
	OPT=$1
else
	echo "Which version of SST do you want to load?"
	echo "No SST version was given..."
	echo "Select one from the list:"
	echo "	0) remove all"
	echo "	1) sst-core+elements"
	OPT=$(zsh -c "read -s -k 1 inp; echo \$inp")
fi

setup $OPT

#echo $MPIHOME
#echo $BOOST_HOME
#echo $SST_HOME
