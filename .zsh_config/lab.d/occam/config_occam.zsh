
SCRIPT_DIRECTORY=$0:h

function multihash2hex()
{
  cd $OCCAM_WS/occam-worker/src
  PROGRAM="
multihash = \"$1\"
from occam.storage.plugins.ipfs_vendor.base58 import b58decode
hashedBytes = b58decode(multihash)
from occam.storage.plugins.ipfs_vendor.multihash import multihash as _multihash
hashedBytes = _multihash.decode(hashedBytes)
import binascii
revision = binascii.hexlify(bytearray(hashedBytes)).decode('utf-8')
print( revision )"
  echo $PROGRAM | python -
  popd &>/dev/null
}


function get_object()
{
	UUID=$1
	FIRST=`echo $UUID|cut -c1,2`
	SECOND=`echo $UUID|cut -c3,4`
	DEST=$2
	git clone http://91.121.76.161:9292/$UUID $DEST
	mkdir -p notes/$FIRST/$SECOND/$UUID/versions
	rsync --progress -rahu -e "ssh -i ~/.ssh/wilkie_dev_server" wilkie@91.121.76.161:/home/wilkie/.occam/notes/$FIRST/$SECOND/$UUID/versions/ ./notes/$FIRST/$SECOND/$UUID/versions/
	cd $DEST
	occam objects pull
	occam objects build -r +
	rsync --progress -rahu -e "ssh -i ~/.ssh/wilkie_dev_server" wilkie@91.121.76.161:/home/wilkie/.occam/notes/$FIRST/$SECOND/$UUID/versions/ $OCCAM_ROOT/notes/$FIRST/$SECOND/$UUID/versions/
	cd ..
}

function get_notes()
{
	UUID=$1
	FIRST=`echo $UUID|cut -c1,2`
	SECOND=`echo $UUID|cut -c3,4`
	rsync --progress -rahu -e "ssh -i ~/.ssh/wilkie_dev_server" wilkie@91.121.76.161:/home/wilkie/.occam/notes/$FIRST/$SECOND/$UUID/versions/ $OCCAM_ROOT/notes/$FIRST/$SECOND/$UUID/versions/
}


function search_server_object()
{
	NAME=$1
	ssh wilkie@91.121.76.161 -i ~/.ssh/wilkie_dev_server bash -ic \"occam objects search -j -n $NAME\"
}

function aliases_new()
{
	alias occam-objects="occam objects "
	alias occam-objects-view="occam objects view "
	alias occam-objects-list="occam objects list "
	alias occam-objects-run="occam objects run "
	alias occam-objects-pull="occam objects pull "
	alias occam-objects-search="occam objects search "
	alias occam-accounts="occam accounts "
	alias occam-system="occam system "
	return
}

function setup()
{
	option=$1
	# common stuff
	case $option in
		0)
			echo "Clean!"
			return
		;;

		1)
			export OCCAM_ROOT=$HOME/.occam_old
			export OCCAM_WS=/mnt/big/occam_workspace/old_version
			export OCCAM_BIN_DIR=$OCCAM_WS/occam-worker
			echo "Selected old experiments"
		;;

		2)
			export OCCAM_ROOT=$HOME/.occam_new
			export OCCAM_WS=/mnt/big/occam_workspace/new_version
			export OCCAM_BIN_DIR=$OCCAM_WS/occam-worker/bin
			echo "Selected new experiments"
		;;
		3)
			export OCCAM_ROOT=$HOME/.occam_old
			export OCCAM_WS=/home/luis/occam_workspace
			export OCCAM_BIN_DIR=$OCCAM_WS/occam-worker
			export OCCAM_FPATH=$SCRIPT_DIRECTORY/fpath_old
			export fpath=($OCCAM_FPATH $fpath)
			compinit
			echo "Selected tutorial"
		;;

		4)
			export OCCAM_ROOT=$HOME/.occam_new
			export OCCAM_WS=/home/luis/occam_workspace/new_version
			export OCCAM_BIN_DIR=$OCCAM_WS/occam-worker/bin
			export OCCAM_FPATH=$SCRIPT_DIRECTORY/fpath_new
			export fpath=($OCCAM_FPATH $fpath)
			compinit
			echo "Selected new version"
			aliases_new
		;;

		5)
			export OCCAM_ROOT=$HOME/.occam_federation
			export OCCAM_WS=/home/luis/occam_workspace/federation
			export OCCAM_BIN_DIR=$OCCAM_WS/occam-worker/bin
			export OCCAM_FPATH=$SCRIPT_DIRECTORY/fpath_new
			export PYTHONPATH=$OCCAM_WS/occam-social:$PYTHONPATH
			export fpath=($OCCAM_FPATH $fpath)
			compinit
			echo "Selected federation version"
			aliases_new
		;;

		*)
			echo "Invalid selection."
			return
		;;
	esac

	export PATH=$PATH:$OCCAM_BIN_DIR


}

function cleanup()
{
	echo "Was already setup ... cleaning"
	export PATH=`echo $PATH | sed 's@'":$OCCAM_BIN_DIR"'@@'`
	export PATH=`echo $PATH | sed 's@'"$OCCAM_BIN_DIR:"'@@'`
	export PATH=`echo $PATH | sed 's@'"$OCCAM_BIN_DIR"'@@'`
	export OCCAM_WS=
	export OCCAM_BIN_DIR=
	NEW_FPATH=
	for i in $fpath
	do
		if [[ $i != $OCCAM_FPATH ]]
		then
			NEW_FPATH=($NEW_FPATH $i)
		fi
	done
	export fpath=($NEW_FPATH)
	export OCCAM_FPATH=
	unfunction _occam
	compdef -d occam >/dev/null
}


# Clean up
if [ -n "$OCCAM_WS" ]
then
	cleanup
fi


if [ -n "$1" ]
then
	OPT=$1
else
	echo "Which version of OCCAM do you want to load?"
	echo "No OCCAM version was given..."
	echo "Select one from the list:"
	echo "	0) remove all"
	echo "	1) tutorial"
	echo "	2) new version"
	OPT=$(zsh -c "read -s -k 1 inp; echo \$inp")
fi

setup $OPT
