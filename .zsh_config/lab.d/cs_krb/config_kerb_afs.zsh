function cs-renew-kerb()
{
  kinit lun8@DEPT.CS.PITT.EDU -k -t $_CS_KRB_KEYTAB_LOCATION
  aklog -d -c cs.pitt.edu
}

function _cs-store-passwd()
{
	PASSWORD=$1
	rm -rf $_CS_KRB_KEYTAB_LOCATION
	echo "addent -password -p lun8@DEPT.CS.PITT.EDU -k 1 -e aes256-cts\n$PASSWORD\nwkt $_CS_KRB_KEYTAB_LOCATION\nquit" |ktutil
	chmod 600 $_CS_KRB_KEYTAB_LOCATION
	echo "rkt $_CS_KRB_KEYTAB_LOCATION\nlist\nquit\n" |ktutil
}

function cs-passwd()
{
	echo -n "Enter the old password: "
	read -r -s OP
	echo ""
	echo -n "Enter the new password: "
	read -r -s NP
	echo ""
	echo $OP
	echo $NP
	echo "$OP\n$NP\n$NP\n" | kpasswd lun8@DEPT.CS.PITT.EDU
	_cs-store-passwd "$NP"
}


export _CS_KRB_KEYTAB_LOCATION=$0:h/loliveira.keytab
