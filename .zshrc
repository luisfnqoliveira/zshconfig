# Path to your oh-my-zsh installation.

#INSTALL_OH_MY_ZSH=false

export ZSH="$HOME/.oh-my-zsh"

if [[ $INSTALL_OH_MY_ZSH != "false" ]]
then

	if [ ! -d "$ZSH" ]
	then
		echo "oh-my-zsh was not found in $ZSH."
		echo "I can run this script for you: 'https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh'"
		read -q "USER_INPUT?Can I install oh-my-zsh for you? (y/N) "
		echo ""

		if [[ -z $USER_INPUT || $USER_INPUT == "y" ]]
		then
			sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh) --unattended --keep-zshrc > /dev/null"
		else
			echo ":("
			echo "Uncomment the first line in file $HOME/.zshrc so that I stop asking."
		fi
	fi
fi

source $HOME/.zsh_config/zsh.conf
if [ -d "$ZSH" ]
then
	source $ZSH/oh-my-zsh.sh
fi

